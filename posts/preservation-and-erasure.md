*Beauty is marked by preservation, and ugliness by erasure.*

I've often had occasion to recall these words, spoken by Anil Srivathsan when he taught me for a term at the Asian College of Journalism in 2011, because they express a fundamental truth about city life but also because they're so elegantly worded that I've often wondered if they contain a deeper truth about the human condition. (I'm aware of [the controversial conflation](https://aeon.co/ideas/beauty-is-truth-truth-is-beauty-and-other-lies-of-physics) here.)

I've always aspired to having neat handwriting, but the font I've most preferred -- with pronounced curves and flourishing ascenders, descenders and finials -- also hurts my wrist the most. So at some point after the second or third page in my high-school notebooks, you'll see a quick drop-off after which the font because something less contrived and more fluidic, the size of its apertures and counters dictated by speed instead of aesthetic choices.

At the same time, if for some reason you kept reading, you might notice from the fifth page or so that while individual letters may themselves seem ungainly, even lopsided, the set of all letters in a word and all words on a page repeated over and over produce a pattern that could seem quite beautiful.

I have seen this also with some of my friends' notes and on the web -- with typography whose individual letters appear clunky but when filled out on a page develop an emergent pleasing quality (two examples: Noticia Text and Quattrocento Sans). I have also ~~seen~~ heard this with music -- especially with some of Ilaiyaraaja's rawer tracks like ['*Potri paadadi penne*'](https://www.youtube.com/watch?v=9iSdfLOCY6k) (*Devar Magan*, 1992) and, of course, almost all of dubstep.

All of which prompts a question: if beauty is marked by preservation, do we think things that have been preserved are, just by that measure, beautiful?

Obviously this is a simplistic, majoritarian proposition, dangerous for its encouragement to continue preserving that which has already been deemed worthy of preservation, which claims in its prevalence a justification for its own existence.

Aesthetic choices are believed to play a part in nature as well, especially in theories of evolutionary aesthetics that attempt to explain how *pleasurable* stimuli elicit or suppress evolutionarily favourable behaviour and/or choices.

> Aesthetic pleasure ... is pleasure in just looking at something, or listening to it, or pleasure in contemplating its qualities. Aesthetic pleasure motivates you to keep looking; it doesn't tell you that the object of your contemplation is good for anything other than contemplation.
>
> Activity-focused pleasures can elicit characteristic bodily or behavioural responses. They might express themselves involuntarily in the face by a soft expression or smile. Psychologically, they focus attention on the pleasurable activity; one tends to 'get lost' in the beauty of the starry sky or in Van Gogh's version, and other concerns, including worries and pain, tend to recede. This dreaminess or absorption is mediated by endogenously secreted opioids, which also bring on feelings of pleasure. These effects are characteristic of what we might call physical pleasure, and we should recognise that some intellectual pleasures -- a book, a Sudoku puzzle, friendly conversation -- elicit physical pleasure. And so does aesthetic pleasure.
>
> *Mohan Matthen, *[Aeon](https://aeon.co/essays/how-did-evolution-shape-the-human-appreciation-of-beauty)*, March 24, 2014*

But here too, one's perception of beauty may guide certain courses of action but I haven't seen evidence of the opposite: that is, "it is beautiful, therefore I contemplate it" but not "I contemplate it, therefore it is beautiful".